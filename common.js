const buttons = document.querySelectorAll('.btn');

buttons.forEach(button => {
    button.addEventListener('click', () => {
        button.style.backgroundColor = 'blue';

        buttons.forEach(element => {
            if (element !== button) {
                element.style.backgroundColor = 'black';
            }
        });
    });
});

document.addEventListener('keydown', (e) => {
    const key = e.key.toUpperCase();
    const button = document.querySelector(`[data-key="${key}"]`);
    if (button) {
        button.click();
    }
});addEventListener